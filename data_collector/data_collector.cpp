#include <fstream>
#include <string>
#include <iostream>

using namespace std;

/*  Author:   Kyle Buss
 *  Updated   3/16/19
 *  Program:  This sub-program was used to collect data of the
 *            balls distance in cm from the robot's cameras along
 *            along with the average (x,y) pixel cordinates
 *            from the left and right cameras. It stores the data
 *            into a csv data.txt file to be used to create a spreadsheet.
 */

// function declaration
int menu();

// global variable
int dist = 0;

int main(int argc, char* argv[])
{
  // begin of objTracker.cpp
  string file = "data.csv";
  ofstream myfile;
  myfile.open(file, ios::out | ios::app);
  dist = menu();
  while (dist > 0) {
    myfile << dist << endl; // << "," << avgLX << "," << avgLY << "," << avgRX << "," << avgRY << "," << xDiff << "," << yDiff << "\n";
    dist = menu();
  }
  myfile.close();
  return 0;
}

int menu() {
  printf("Enter the ball's distance from robot in cm (enter 0 to quit):\n");
  scanf("%d", &dist);

  // TODO: code for main menu to be added later
  // int select = 0;
  // char pressed = '';
  // printf("Good day Dr. Trantham, what would you like to do today?\n");
  // printf("Press A to collect data.\n");
  // printf("Press Q to quit.");
  // scanf("%c\n", &pressed);
  // if(pressed == 'a' || pressed == 'A') {
  //   select = 1;
  // }
  return dist;
}
