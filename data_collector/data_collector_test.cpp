#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

/*  Author:   Kyle Buss
 *  Date:     3/13/19
 *  Program:  This test program was used to test the output of the data_collector.cpp
 *            code. The result of test will be the data_collector_test.txt file with
 *            the values from dist[], avgLX[], avgLY[], avgRX[], avgRY[], xDiff[], and
 *            yDiff[]. It then tests the values from the file against the original values
 *            to determine success or failure.
 */

int main(int argc, char* argv[]) {
// Test data to be writen to file
  int dist [] = {60, 80, 100, 120, 140, 160, 200, 220, 240, 260, 280, 300};
  int avgLX [] = {201, 209, 216, 222, 229, 236, 243, 252, 263, 272, 279, 286};
  int avgLY [] = {260, 248, 233, 215, 203, 189, 180, 172, 166, 160, 152, 148};
  int avgRX [] = {300, 291, 283, 275, 280, 285, 290, 295, 300, 306, 310, 314};
  int avgRY [] = {260, 248, 233, 215, 203, 189, 180, 172, 166, 160, 152, 148};
  int xDiff[12];
  int yDiff[12];
// open or create new file named data_collector_test.txt
  string file = "data_collector_test.txt";
// output fstream with write and append capabilities
  ofstream myfile;
  myfile.open(file, ios::out | ios::app);
// loop through test data to calculate difference between left and right x,y averages
// print all data to .txt file.
  for(int i = 0; i < 12; i++) {
    xDiff[i] = avgRX[i] - avgLX[i];
    yDiff[i] = avgRY[i] - avgLY[i];
    myfile << dist[i] << "," << avgLX[i] << "," << avgLY[i] << "," << avgRX[i] << "," << avgRY[i] << "," << xDiff[i] << "," << yDiff[i] << endl;
  }
// close the output file
  myfile.close();

// create array to read data from newly created file into
  int fileTest[12][7];

// open created file
  ifstream readMyFile(file, ios::in);
  string line;
// loop through file and store data from file into array
  for(int i = 0 ; i < 12; i++) {
      getline(readMyFile, line);
        stringstream lines(line);
        string data;
      for(int j = 0; j < 7; j++) {
        getline(lines, data, ',');
        fileTest[i][j] = stoi(data);
      }
  }
// close input file
  readMyFile.close();

// loop through read file data and compare to original arrays to check values match.
    for(int i = 0; i < 12; i++) {
      // if values do not match, return test failed.
        if(fileTest[i][0] != dist[i] || fileTest[i][1] != avgLX[i] || fileTest[i][2] != avgLY[i]
            || fileTest[i][3] != avgRX[i] || fileTest[i][4] != avgRY[i] || fileTest[i][5] != xDiff[i]
            || fileTest[i][6] != yDiff[i]) {
              printf("Test %d has FAILED!\n", i + 1);
            } else {
              // if values match, return test passed.
              printf("Test %d has PASSED!\n", i + 1);
            }
      }
  return 0;
}
