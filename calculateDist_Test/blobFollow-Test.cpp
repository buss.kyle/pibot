/*  Author:   Kyle Buss
 *  Date:     3/31/19
 *  Program:  This program is a unit test for blobFollow.cpp it returns the calculated
 *            distance to verify that the function works. Values used are from Data.xlsx.
 */
 #include <iostream>
 #include <fstream>
 #include <math.h>
 #include <string>

 using namespace std;

 float calculateDist(int);

 const float K1 = 3998.12f;
 const float K2 = -0.87f;

 int main(int argc, char* argv[]) {
   string pass = "Test Passed: Calculated Distance = Test Distance +/- 15 cm.";
   string fail = "Test Failed";
   ifstream inFile;
   int test = 1;
   // Test dist will be used to compare returned value +/- 10 cm of expected value.
   // Actual value is 60 cm, but anything +/- 10 cm of 60 is considered 0 or very close.
   float testDist;
   // Test xChange is the test value for difference in Lx and Rx from detectObj()
   int xChange;

   inFile.open("followTestData.txt", ios::in);
   while (!inFile.eof()) {
     if(test > 21 ) {
       break;
     }
     inFile >> testDist >> xChange;
     float calculatedDist = calculateDist(xChange);
     if (calculatedDist >= (testDist - 15) && calculatedDist <= (testDist + 15)) {
       printf("%d. %s\n Calculated value = %.2f\n Test value = %.2f\n Change between Lx and Rx = %d\n", test, pass.c_str(), calculatedDist, testDist, xChange);
     } else {
       printf("%d. %s\n Calculated value = %.2f\n Test value = %.2f\n Change between Lx and Rx = %d\n", test, fail.c_str(), calculatedDist, testDist, xChange);
     }
     test++;
   }

   return 0;
 }

 float calculateDist(int changeX) {
   // calcDist set to 0.0 means object is really close.
   float calcDist = 0.0;
   if (changeX < 10) {
     // calcDist set to INFINITY means object is far away
     calcDist = INFINITY;
   } else if (changeX >= 10 && changeX <= 100) {
     // physics calculation to determine objects distance from bot...
     calcDist = K1 * pow(changeX, K2);
   }
   return calcDist;
 }
